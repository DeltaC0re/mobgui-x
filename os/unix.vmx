--[[
    -- @Name: MobGui
	-- @Description: System core
	-- @Author: Marcel Benning
	-- @Usage: startup
	-- @Version: 2.0
	-- @Web: http://ccmob.net
	-- @Buildnumber: 40
--]]

--[[System Vars]]--
MOBGUI__VERSION = "2.0@Beta"
MOBGUI__BUILDNO = 40
MOBGUI__UPDATE = false
MOBGUI__UPDATE__SILENT = false
OS_PATH = "/unix/"
WEB_PATH = "https://mb.app.ccmob.net/mobgui-x/"
TMP_PATH = "/unix/cache/"
APIS_PATH = "/unix/lib/apis/"
DRIVER_PATH = "/unix/driver/"
DRIVER_COMPILED_PATH = "/unix/driver/compiled/"
APPS_PATH = "/unix/usr/"
MOBDB_PATH = "/unix/lib/db.d/systemdb"
SOURCEDB_PATH = "/unix/lib/db.d/sourcedb"
MODULES_PATH = "/unix/lib/modules/"
TERM_SIZE_X = 0
TERM_SITE_Y = 0
TERM_SIZE_X, TERM_SIZE_Y = term.getSize()
COLOR_MESSAGEBOX_TITLEBAR = colors.gray
COLOR_MESSAGEBOX_TITLE = colors.white
COLOR_MESSAGEBOX_BACKGROUND = colors.lightGray
COLOR_MESSAGEBOX_TEXT = colors.white
DEBUG_C = 0
SYSSTAR_FLAG = true
MOBDB_VERSION = "2.0"
system_monitor = nil
bootScreen = false
MOBDB = nil
SOURCEDB = nil

--[[
	-- Database Keys
]]--

local __DB_KEY_UNIX_LIB_APIS_LOAD_DISPLAY = "unix.lib.apis.load.display"
local __DB_KEY_UNIX_LIB_MODULES_LOAD_DISPLAY = "unix.lib.modules.load.display"
local __DB_KEY_UNIX_BOOT_CLEAR_AFTER_DONE = "unix.boot.clear"

local __DB_KEY_UNIX_BOOT_AUTOSTART = "unix.boot.autostart"
local __DB_KEY_UNIX_BOOT_AUTOSTART_PATH = "unix.boot.autostart.path"
local __DB_KEY_UNIX_BOOT_AUTOSTART_DELAY = "unix.boot.autostart.delay"

__DB_KEY_YAOURT_DISPLAY_INSTALL_SOURCES = "app.yaourt.install.sources.display"

CMD_HISTORY = {}

function executeCommand() 
end -- function prototype
downloadApis = nil

--term.clear()
--term.setCursorPos(1,1)

-- [[ HTTP api extension ]] --

function http.downloadFile(url, destination) --Nano:True
    if fs.exists(destination) then
        fs.delete(destination)
    end
	handle = http.get(url)
	if handle then
		file = io.open(destination,"w")
		file:write(handle.readAll())
		file:close()
		return true
	else
		return false
	end
end

function http.downloadString(url) --Nano:True
	handle = http.get(url)
	if handle then
		return handle.readAll()
	else
		return nil
	end
end

-- [[ Version Check ]] --

local function split(pString, pPattern) -- local split function
   local Table = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pPattern
   local last_end = 1
   local s, e, cap = pString:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
     table.insert(Table,cap)
      end
      last_end = e+1
      s, e, cap = pString:find(fpat, last_end)
   end
   if last_end <= #pString then
      cap = pString:sub(last_end)
      table.insert(Table, cap)
   end
   return Table
end

print("Getting version information ...")

local serverVersion = http.downloadString(WEB_PATH .. "version")
local serverVersionNumber = 0
local localVersionNumber = 0
print("Server version: " .. serverVersion)

if string.find(serverVersion, "@") then
	local serverVersionA = split(serverVersion, "@")
    serverVersionNumber = serverVersionA[1]
    --local serverVersionString = serverVersionA[2]
end
if string.find(MOBGUI__VERSION, "@") then
	local localVersionA = split(MOBGUI__VERSION, "@")
    localVersionNumber = localVersionA[1]
    --local localVersionString = localVersionA[2]
end
if tonumber(serverVersionNumber) > tonumber(localVersionNumber) then
	MOBGUI__UPDATE = true
end
local serverBuild = http.downloadString(WEB_PATH .. "build")
if tonumber(serverBuild) > tonumber(MOBGUI__BUILDNO) then
	MOBGUI__UPDATE__SILENT = true	
end

if MOBGUI__UPDATE then
	print("Update found ...")
end

local exitMsg = getExitMsg()
if exitMsg == "force-upgrade" then
	MOBGUI__UPDATE = true
	MOBGUI__UPDATE__SILENT = true
end

-- [[ Module functionaltiy ]] --

function Class(className)
	local c = {}
	c.className = className
	return c
end

_Apis = {}
_loadedApis = {}

function defineAPI(apiName, localPath, remotePath, osLoad, capi)
	local _api = Class("mobgui.api")
	_api.localPath = APIS_PATH .. localPath
	_api.remotePath = remotePath
	_api.name = apiName
	_api.useOsLoad = osLoad
	_api.loaded = false
	_api.forceLoad = false
	if capi then
		_api.forceLoad = true	
	end
	function _api.loadExtension()
		if _api.loaded == false then
			_api.loaded = true
			if MOBGUI__UPDATE or MOBGUI__UPDATE__SILENT or _api.forceLoad then
				if fs.exists(_api.localPath) then
					fs.delete(_api.localPath)
				end
				if http.downloadFile(_api.remotePath, _api.localPath) then
					if _api.useOsLoad ~= nil and _api.useOsLoad == true then
						os.loadAPI(_api.localPath)
					else
						shell.run(_api.localPath)
					end
					return true, false
				else
					if(fs.exists(_api.localPath .. ".bak")) then
						fs.move(_api.localPath .. ".bak", _api.localPath)
						return true, false
					else
						return false, false
					end
				end
			else
				if fs.exists(_api.localPath) == false then
					http.downloadFile(_api.remotePath, _api.localPath)
				end
				if fs.exists(_api.localPath) then
					if _api.useOsLoad ~= nil and _api.useOsLoad == true then
						os.loadAPI(_api.localPath)
					else
						shell.run(_api.localPath)
					end
					return true, false
				end
			end
		else
			return true, true
		end
	end
	_Apis[#_Apis+1] = _api
	return _api
end

function loadAPIs()
	printDebugInfo("Loading Apis ...")
	fs.delete(APIS_PATH)
	fs.makeDir(APIS_PATH)
	local displayType = DBGetValue(__DB_KEY_UNIX_LIB_APIS_LOAD_DISPLAY, MOBDB) 
	if displayType == "list" then
		for index, _api in ipairs(_Apis) do
			local modElement = printDebug(_api.name)
			local succ, al = _api.loadExtension()
			if al then
				printDebugDone(modElement)
			else	
				if succ then
					printDebugOk(modElement)
				else
					printDebugFail(modElement)
				end	
			end
		end
	elseif displayType == "bar" then
		local x,y = term.getCursorPos()
		local width, height = term.getSize()
		local barLength = width - 2
		local barCharDone = "#"
		local barCharFail = "X"
		local barCharWait = "."
		local bar = "[" .. string.rep(barCharWait, barLength) .. "]"
		term.setCursorPos(1, y)
		term.setTextColor(colors.white)
		term.write(bar)
		term.setCursorPos(2, y)
		local apisLength = #_Apis
		local apisCounter = 0
		local tileLength = barLength / apisLength
		local function updateToBar(state)
			if state then
				term.setTextColor(colors.green)
				term.write(string.rep(barCharDone, tileLength))
			else
				term.setTextColor(colors.red)
				term.write(string.rep(barCharDone, tileLength))
			end
			term.setTextColor(colors.white)
		end
		for index, _api in ipairs(_Apis) do
			apisCounter = apisCounter + 1
			local succ, al = _api.loadExtension()
			if al then
				updateToBar(true)
			else	
				if succ then
					updateToBar(true)
				else
					updateToBar(false)
				end	
			end
			if apisCounter == #_Apis then
				local xp, yp = term.getCursorPos()
				if xp <= width - 1 then
					term.setTextColor(colors.green)
					term.write(string.rep(barCharDone, width - xp))
				end
				term.setTextColor(colors.white)
			end
		end
		term.setCursorPos(1, y+1)
	else
		for index, _api in ipairs(_Apis) do
			_api.loadExtension()
		end
	end
end

_Modules = {}

function loadModules()
	local displayType = DBGetValue(__DB_KEY_UNIX_LIB_MODULES_LOAD_DISPLAY, MOBDB)
	
	printDebugInfo("Loading CoreMods ...")
	
	if displayType == "list" then
		for index, _mod in ipairs(_Modules) do
			local modElement = printDebug(_mod.name)
			if _mod.loadExtension() then
				printDebugOk(modElement)
			else	
				printDebugFail(modElement)
			end
		end
	elseif displayType == "bar" then
		local x,y = term.getCursorPos()
		local width, height = term.getSize()
		local barLength = width - 2
		local barCharDone = "#"
		local barCharFail = "X"
		local barCharWait = "."
		local bar = "[" .. string.rep(barCharWait, barLength) .. "]"
		term.setCursorPos(1, y)
		term.setTextColor(colors.white)
		term.write(bar)
		term.setCursorPos(2, y)
		local modulesLength = #_Modules
		local modulesCounter = 0
		local tileLength = barLength / modulesLength
		local function updateToBar(state)
			if state then
				term.setTextColor(colors.green)
				term.write(string.rep(barCharDone, tileLength))
			else
				term.setTextColor(colors.red)
				term.write(string.rep(barCharDone, tileLength))
			end
			term.setTextColor(colors.white)
		end
		for index, _mod in ipairs(_Modules) do
			modulesCounter = modulesCounter + 1
			if _mod.loadExtension() then
				updateToBar(true)
			else	
				updateToBar(false)
			end
			if modulesCounter == #_Modules then
				local xp, yp = term.getCursorPos()
				if xp <= width - 1 then
					term.setTextColor(colors.green)
					term.write(string.rep(barCharDone, width - xp))
				end
				term.setTextColor(colors.white)
			end
		end
		term.setCursorPos(1, y+1)
	else
		for index, _mod in ipairs(_Modules) do
			_mod.loadExtension()
		end
	end
	
	--[[ for index, _mod in ipairs(_Modules) do
		local x,y = term.getCursorPos()
		if verbose then
			local modElement = printDebug(_mod.name)
		end
		if _mod.loadExtension() then
			if verbose then
				printDebugOk(modElement)
			end
		else	
			if verbose then
				printDebugFail(modElement)
			end
		end
	end]]--
end

function defineModule(moduleName, _path)
	local _mod = Class("mobgui.module")
	_mod.localPath = MODULES_PATH .. _path
	_mod.remotePath = WEB_PATH .. "modules/" .. _path
	_mod.name = moduleName
	_mod.loaded = false
	function _mod.loadExtension()
		if not _mod.loaded then 
			_mod.loaded = true
			if MOBGUI__UPDATE or MOBGUI__UPDATE__SILENT then
				if fs.exists(_mod.localPath) then
					fs.delete(_mod.localPath)
				end
				if http.downloadFile(_mod.remotePath, _mod.localPath) then
					shell.run(_mod.localPath)
					return true
				else
					if(fs.exists(_mod.localPath .. ".bak")) then
						fs.move(_mod.localPath .. ".bak", _mod.localPath)
						return true
					else
						return false
					end
				end
			else
				if fs.exists(_mod.localPath) == false then
					http.downloadFile(_mod.remotePath, _mod.localPath)
				end
				if fs.exists(_mod.localPath) then
					shell.run(_mod.localPath)
					return true
				end
			end
		else
			return true
		end
	end
	_Modules[#_Modules+1] = _mod
	return _mod
end

_ModPostFunctions = {}

function coreModQueuePostF(modName, f)
	local _fHandler = Class("mobgui.module.postexec")
	_fHandler.name = modName
	_fHandler.f = f
	function _fHandler.postLoad()
		if type(_fHandler.f) == "function" then	
			_fHandler.f()
		end
	end
	_ModPostFunctions[#_ModPostFunctions+1] = _fHandler
	return _fHandler
end

function postLoadModules()
	printDebugInfo("Post loading CoreMods ...")
	for index, _mod in ipairs(_ModPostFunctions) do
		_mod.postLoad()
	end
end

_CustomModules = {}

function loadCModules()
	printDebugInfo("Loading CustomMods ...")
	for index, _mod in ipairs(_CustomModules) do
		local x,y = term.getCursorPos()
		local modElement = printDebug(_mod.name)
		if _mod.loadExtension() then
			printDebugOk(modElement)
		else
			printDebugFail(modElement)
		end
	end	
end

function defineCModule(moduleName, _path, remotePath)
	local _mod = Class("mobgui.module.custom")
	_mod.localPath = MODULES_PATH .. _path
	_mod.remotePath = remotePath
	_mod.name = moduleName
	function _mod.loadExtension()
		if fs.exists(_mod.localPath) then
			if fs.exists(_mod.localPath .. ".bak") then
				fs.delete(_mod.localPath .. ".bak")	
			end
			fs.move(_mod.localPath, _mod.localPath .. ".bak")
		end
		if http.downloadFile(_mod.remotePath, _mod.localPath) then
			shell.run(_mod.localPath)
			return true
		else
			if(fs.exists(_mod.localPath .. ".bak")) then
				fs.move(_mod.localPath .. ".bak", _mod.localPath)
				return true
			else
				return false
			end
		end
	end
	_CustomModules[#_CustomModules+1] = _mod
	return _mod
end

print("Loading system modules")
fs.delete(MODULES_PATH)
local terminalMod = defineModule("TerminalMod", "Terminal.msx")
terminalMod.loadExtension()
local stringMod = defineModule("StringMod", "string.msx")
local tableMod = defineModule("TableMod", "table.msx")
local metaFile = defineModule("meta", "meta.msx")
stringMod.loadExtension()
tableMod.loadExtension()
metaFile.loadExtension()
local mobdbMod = defineModule("MobDB", "mobdb.msx")
mobdbMod.loadExtension()
local fsMod = defineModule("FSMod", "FS.msx")
local systemCatchMod = defineModule("SysCatchMod", "SystemCatch.msx")
local peripheralMod = defineModule("PeripheralMod", "peripherals.msx")
local guiMod = defineModule("GuiMod", "gui.msx")
local configMod = defineModule("ConfigMod", "Config.msx")
local appMod = defineModule("AppMod", "App.msx")
local commandParserMod = defineModule("Command parser", "CommandParser.msx")
local commandParserInstall = defineModule("CP - install", "CP_install.msx")
local jsonLoader = defineModule("JsonApiLoader", "ApiModule.msx")
local mobUpdater = defineModule("MobGuiUpdater", "MobGuiUpdater.msx")
local rcMod = defineModule("RC-Scripts", "rclocal.msx")
local defaultsMod = defineModule("defaults", "defaults.msx")
loadModules()
loadCModules()
loadAPIs()

function createFolders() --Nano:False
	setupDir = printDebug("Setting up directories ...")
	local folder = {
		OS_PATH,
		TMP_PATH,
		APIS_PATH,
		DRIVER_PATH,
		DRIVER_COMPILED_PATH,
		APPS_PATH,
		MOBDB_PATH,
		SOURCEDB_PATH,
		MODULES_PATH
	}
	for i = 1 , #folder do
		if not fs.exists(folder[i]) then
			printDebugInfo("Creating dir :" .. folder[i])
			fs.makeDir(folder[i])
		end
	end
	printDebugOk(setupDir)
end

function downloadProgramms() --Nano:False
	local defaultPrograms = {
		"addRepo",
		"dbi"
	}
	for _,pgm in ipairs(defaultPrograms) do
		if not isAppInstalled(pgm) then
			ioparseCommand("yaourt " .. pgm, false)	
		end
	end
	return
end

local function addAlias(alias, path) --Nano:False
	shell.setAlias( alias, path )
end

function setupAliases() --Nano:False
	-- Adding default alias
	addAlias( "ls", "list" )
	addAlias( "dir", "list" )
	addAlias( "cp", "copy" )
	addAlias( "mv", "move" )
	addAlias( "rm", "delete" )
	addAlias( "preview", "edit" )
	-- -- -- -- -- -- -- --
	local totalAlias = 5
	for id, file in ipairs(fs.list(APPS_PATH)) do
		if fs.isDir(file) then
		else
			addAlias(replace(file,".msx",""),APPS_PATH .. file)
			totalAlias = totalAlias + 1
		end
	end
	if SYSSTAR_FLAG then
		printDebugInfo("Total Userprogramms found " , totalAlias)
	end
end

function isAppInstalled(appName) --Nano:True
	return fs.exists(APPS_PATH .. tostring(appName) .. ".msx")
end

function requireApp(appName, repo)
	if repo then
		if not isRepositoryAvailable(repo) then
			ioparseCommand("addRepo " .. repo .. " -s", false) 	
		end
	end
	if not isAppInstalled(appName) then
		ioparseCommand("yaourt " .. appName , false) 	
	end
end

function isRepositoryAvailable(url) --Nano:True
	local repo = url
	if endsWith(repo, ".html") or endsWith(repo, ".php") or endsWith(repo, "/") then
	else
		repo = repo .. "/"
	end
	sourcedb = DBload(SOURCEDB_PATH)
	flag = false
	for i = 1, DBGetItemCount(sourcedb) do
	    if(DBGetValue(tostring(i), sourcedb) == repo) then
	        flag = true
	    end
	end
	return flag
end
-- overrides executeCommand with the command parser one
-- override this function to get all user input, you will not be easily able to use mobgui commands anymore
--  except if you use ioparseCommand

--Only CraftOS 1.6 !
local function createTab(input) --Nano:False
	local tArgs = split(input, " ")
	if #tArgs > 0 then
	    local nTask = shell.openTab( unpack( tArgs ) )
	    if nTask then
	        shell.switchTab( nTask )
	    end
	else
	    local nTask = shell.openTab( "shell" )
	    if nTask then
	        shell.switchTab( nTask )
	    end
	end
end

function enableStartupFlag() --Nano:False
	SYSSTAR_FLAG = true
end

function disableStartupFlag() --Nano:False
	SYSSTAR_FLAG = false
end

function setDebugLevel(l) --Nano:True
	DEBUG_C = l
end

setDebugLevel(2)

IS_RUNNING = true
DBAddDefault("1", WEB_PATH .. "cur/", SOURCEDB)
DBAddDefault(__DB_KEY_YAOURT_DISPLAY_INSTALL_SOURCES, "false", MOBDB)
DBAddDefault(__DB_KEY_UNIX_LIB_APIS_LOAD_DISPLAY, "none", MOBDB)
DBAddDefault(__DB_KEY_UNIX_LIB_MODULES_LOAD_DISPLAY, "none", MOBDB)
DBAddDefault(__DB_KEY_UNIX_BOOT_CLEAR_AFTER_DONE, "false", MOBDB)
DBAddDefault(__DB_KEY_UNIX_BOOT_AUTOSTART, "false", MOBDB)
DBAddDefault(__DB_KEY_UNIX_BOOT_AUTOSTART_PATH, "/autostart.msx", MOBDB)
DBAddDefault(__DB_KEY_UNIX_BOOT_AUTOSTART_DELAY, "1", MOBDB)
sysErr = CreateSystemCatch()
createFolders()
downloadProgramms()

setupAliases()

postLoadModules()

disableStartupFlag()

doAutostart = DBGetValue(__DB_KEY_UNIX_BOOT_AUTOSTART, MOBDB) == "true"
printDebugInfo("Autostart enabled: " .. tostring(doAutostart))
if doAutostart then
	if DBGetValue(__DB_KEY_UNIX_BOOT_AUTOSTART_PATH, MOBDB) then
		if(DBGetValue(__DB_KEY_UNIX_BOOT_AUTOSTART_DELAY, MOBDB)) then
			sleep(tonumber(DBGetValue(__DB_KEY_UNIX_BOOT_AUTOSTART_DELAY, MOBDB)))
		end
		shell.run(DBGetValue(__DB_KEY_UNIX_BOOT_AUTOSTART_PATH, MOBDB))
	end
end

if sysErr.hasException() then
	sysErr.printOut()
end

setExitMsg("")

term.setTextColor(colors.white)
term.setBackgroundColor(colors.black)

if (DBGetValue(__DB_KEY_UNIX_BOOT_CLEAR_AFTER_DONE, MOBDB) == "true") then
	term.setTextColor(colors.white)
	term.setBackgroundColor(colors.black)
	term.setCursorPos(1,1)
	shell.run("clear")
end
term.setTextColor(colors.blue)
print("MobGui V" .. MOBGUI__VERSION .. "[" .. MOBGUI__BUILDNO .. "]")
term.setTextColor(colors.white)
setDebugLevel(1)

while IS_RUNNING do
    saveColor(colors.yellow)
    term.write(shell.dir())
    term.write("> ")
    saveColor(colors.white)
	input = read(nil,CMD_HISTORY, shell.complete)
    executeCommand(input)
end