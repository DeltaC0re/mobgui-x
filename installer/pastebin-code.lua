function printFormated(braces, color, text)
    term.setTextColor(colors.white)
    term.setBackgroundColor(colors.black)
    term.write("[")
    term.setTextColor(color)
    term.write(braces)
    term.setTextColor(colors.white)
    term.write("] ")
    print(text)
end

-- [[ HTTP api extension ]] --

function http.downloadFile(url, destination) --Nano:True
	handle = http.get(url)
	if handle then
		file = io.open(destination,"w")
		file:write(handle.readAll())
		file:close()
		return true
	else
		return false
	end
end

function http.downloadString(url) --Nano:True
	handle = http.get(url)
	if handle then
		return handle.readAll()
	else
		return nil
	end
end

-- [[ Variables ]] --

HTTP_REPO = "https://bitbucket.org/DeltaC0re/mobgui-x/raw/master/"

printOk=function(text) printFormated("Ok", colors.green, text) end
printFail=function(text) printFormated("Fail", colors.red, text) end
printInfo=function(text) printFormated("Info", colors.lightBlue, text) end

-- [[ Actual source ]] --

printInfo("Loading installer V0.1")

if fs.exists("/.preinstaller") then
    printInfo("Doing cleanup ...")
    fs.delete("/.preinstaller")    
end

http.downloadFile(HTTP_REPO .. "installer/installer.lua", "/.preinstaller")

if not fs.exists("/.preinstaller") then
    printFail("Unable to contact bitbucket servers to execute os installer")
else
    printInfo("Executing installer...")
    dofile("/.preinstaller")
end

if fs.exists("/.preinstaller") then
    printInfo("Doing cleanup ...")
    fs.delete("/.preinstaller")    
end

printOk("Installer finished")