function printFormated(braces, color, text)
    term.setTextColor(colors.white)
    term.setBackgroundColor(colors.black)
    term.write("[")
    term.setTextColor(color)
    term.write(braces)
    term.setTextColor(colors.white)
    term.write("] ")
    print(text)
end

function printFormatedStay(braces, color, text)
    term.setTextColor(colors.white)
    term.setBackgroundColor(colors.black)
    term.write("[")
    term.setTextColor(color)
    term.write(braces)
    term.setTextColor(colors.white)
    term.write("] ")
    term.write(text)
end

function printFormatedStayClose(text, color)
    term.setTextColor(color)
    print(" " .. text)
    term.setTextColor(colors.white)
end

printOk=function(text) printFormated("Ok", colors.green, text) end
printFail=function(text) printFormated("Fail", colors.red, text) end
printInfo=function(text) printFormated("Info", colors.lightBlue, text) end

printOkS=function(text) printFormatedStay("Ok", colors.green, text) end
printFailS=function(text) printFormatedStay("Fail", colors.red, text) end
printInfoS=function(text) printFormatedStay("Info", colors.lightBlue, text) end

function cleanup()
    for index, file in pairs(fs.list("/.installer")) do
        fs.delete("/.installer/" .. file)
        printInfo("Removing file '/.installer/" .. file .. "'")
    end
end

function exitWithError(text)
    printFail(text)
    --cleanup()
    --shell.exit(1)
end

function readFile(path)
    if fs.exists(path) then
        local content = ""
        local f = io.open(path, "r")
        if f then
            for line in f:lines() do
                content = content .. line 
            end
            f:close()
            return content
        else
            return nil
        end
    else
    end
end

-- [[ HTTP api extension ]] --

function http.downloadFile(url, destination) --Nano:True
    if fs.exists(destination) then
        fs.delete(destination)
    end
	handle = http.get(url)
	if handle then
		file = io.open(destination,"w")
		file:write(handle.readAll())
		file:close()
		return true
	else
		return false
	end
end

function http.downloadString(url) --Nano:True
	handle = http.get(url)
	if handle then
		return handle.readAll()
	else
		return nil
	end
end


function comb(v1, v2)
    return fs.combine(v1, v2)
end

-- [[ Variables ]] --

HTTP_REPO = "https://mb.app.ccmob.net/mobgui-x/"

INSTALLER_DIR = "/.installer/"

INSTALLER_MANIFEST = comb(INSTALLER_DIR, "manifest")
JSON_LIB = comb(INSTALLER_DIR, "json")

printInfo("Getting json library... [" .. JSON_LIB .. "]")
if not http.downloadFile("http://pastebin.com/raw/4nRg9CHU", JSON_LIB) then
    exitWithError("Unable to retrieve installation manifest !")
end

os.loadAPI(JSON_LIB)

if json == nil then
    exitWithError("Unable to load json library") 
end

printInfo("Retrieving installation manifest ...")
fs.makeDir(".installer")
if not http.downloadFile(HTTP_REPO .. "installer/install.json", INSTALLER_MANIFEST) then
    exitWithError("Unable to retrieve installation manifest !")
end

local manifest_content = readFile(INSTALLER_MANIFEST)
if manifest_content then
    --print(manifest_content)
    local manifest = json.decode(manifest_content)

    for i, obj in pairs(manifest.commands_pre) do
        printInfo("Executing '" .. obj .. "'")
        shell.run(obj)
    end
    
    for index, repo_file in pairs(manifest.repo_files) do
        printInfoS("Fetch[" .. index .. "] - " .. repo_file.repoURL)
        if not http.downloadFile(manifest.repo_base .. repo_file.repoURL, repo_file.localURL) then
            printFormatedStayClose("Fail", colors.red)
        else
            printFormatedStayClose("Ok", colors.green)
        end
    end
    
    for i, obj in pairs(manifest.commands_after) do
        printInfo("Executing '" .. obj .. "'")
        shell.run(obj)
    end
    
else
    exitWithError("Unable to read manifest file") 
end

--cleanup()