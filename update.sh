#!/bin/bash

##################################
#   -- @Name: MobGui build tool
#	-- @Description: Updates version and build numbers and pushes to git repo
#	-- @Author: Marcel Benning
#	-- @Usage: sh run.sh
#	-- @Version: 1.0
#	-- @Web: http://ccmob.net
##################################

bash log.sh "Getting old build number..."
typeset -i buildno=$(cat build)
bash log.sh "Old build number: $buildno"
buildnon=$((buildno+1))
bash log.sh "New build number: $buildnon"

#bash scripts/log.sh "Replacing old build numbers in initstartup.msx ..."
#cat initstartup.msx | sed -e "s/MOBGUI__BUILDNO = $buildno/MOBGUI__BUILDNO = $buildnon/" > initstartup.tmp
#mv initstartup.tmp initstartup.msx
#cat initstartup.msx | sed -e "s/	-- @Buildnumber: $buildno/	-- @Buildnumber: $buildnon/" > initstartup.tmp
#mv initstartup.tmp initstartup.msx

bash log.sh "Replacing old build numbers in initstartup.msx ..."
cat os/unix.vmx | sed -e "s/MOBGUI__BUILDNO = $buildno/MOBGUI__BUILDNO = $buildnon/" > os/unix.vmx.tmp
mv os/unix.vmx.tmp os/unix.vmx
cat os/unix.vmx | sed -e "s/	-- @Buildnumber: $buildno/	-- @Buildnumber: $buildnon/" > os/unix.vmx.tmp
mv os/unix.vmx.tmp os/unix.vmx

bash log.sh "Done."
bash log.sh "Saving new build number: $buildnon"
echo "$buildnon" > build
bash log.sh "Pushing to bitbucket..."
echo "$# $@";
git add --all *
git add -u
if [ "$#" -eq 0 ]
then
    git commit
else
    git commit -m "$@"
fi
git push
bash log.sh "Done."