<?php

$apis = array();

function addApi($name, $remotepath, $localName, $osload){
    global $apis;
    array_push($apis, array("apiName" => "$name","localName" => "$localName","remotePath" => "$remotepath","osLoad" => $osload));
}

$base = "https://mb.app.ccmob.net/mobgui-x/apis/";

addApi("debug", "$base" . "debug", "debug", true);
addApi("database", "$base" . "database", "database", true);
addApi("drawapi", "$base" . "drawapi", "drawapi", true);
addApi("httpapi", "$base" . "httpapi", "httpapi", true);
addApi("networkapi", "$base" . "networkapi", "networkapi", true);
addApi("termapi", "$base" . "termapi", "termapi", true);
addApi("args", "$base" . "args", "args", true);

echo json_encode($apis);

?>