<?php

$WEB_PATH = "https://mb.app.ccmob.net/mobgui-x/cur/";
$DATA_PATH = $WEB_PATH . "data/";

class Dependency{
    function __construct($pName, $pVersion, $repo ,$dependencies = array()){
        $this->pName = $pName;
        $this->pVersion = $pVersion;
        $this->dependencies = $dependencies;
        $this->repo = $repo;
    }
}
class Program{
    function __construct($pName, $pVersion, $remotePath, $rawFileName , $repo ,$dependencies = array()){
        $this->pName = $pName;
        $this->pVersion = $pVersion;
        $this->remotePath = $remotePath;
        $this->dependencies = $dependencies;
        $this->rawFileName = $rawFileName;
        $this->repo = $repo;
    }
}

$programs = array();

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

function println($text){
    //print($text . "\n");
}

function parseFile($filename, $path){
    global $programs;
    global $WEB_PATH;
    global $DATA_PATH;
    $handle = fopen($path . $filename, "r");
    if ($handle) {
        $headerFlag = false;
        $name = "notset_" . $filename;
        $descrption = "notset";
        $author = "notset";
        $usage = "notset";
        $version = "notset";
        $web = "notset";
        while (($line = fgets($handle)) !== false) {
            println("Line: " . trim($line));
            if(!$headerFlag){
                if(startsWith(trim($line), "--[[")){
                    $headerFlag = true;
                    println("Found start");
                    continue;
                }
            }
            if($headerFlag){
                $line = trim($line);
                if(startsWith($line, "-- @Name: ")){
                    $name = substr($line, 10);
                    println("Name: " . $name);
                }else if(startsWith($line, "-- @Description: ")){
                    $descrption = substr($line, 17);
                    println("Description: " . $descrption);
                }else if(startsWith($line, "-- @Author: ")){
                    $author = substr($line, 12);
                    println("Author: " . $author);
                }else if(startsWith($line, "-- @Usage: ")){
                    $usage = substr($line, 11);
                    println("Usage: " . $usage);
                }else if(startsWith($line, "-- @Version: ")){
                    $version = substr($line, 13);
                    println("Version: " . $version);
                }else if(startsWith($line, "-- @Web: ")){
                    $web = substr($line, 9);
                    println("Web: " . $web);
                }
            }
            if(startsWith($line, "--]]")){
                println("End :)");
                break;
            }
            
        }
        
        array_push($programs, new Program($name, $version, $DATA_PATH . $filename, $filename, $WEB_PATH));
    
        fclose($handle);
    } else {
        // error opening the file.
    } 
    
}

$path = "./data/";

if ($handle = opendir($path)) {
    while (false !== ($file = readdir($handle))) {
        if ('.' === $file) continue;
        if ('..' === $file) continue;

        parseFile($file, $path);
    }
    closedir($handle);
}

$trc = "https://git.ccmob.net/dennis/Turbine-Reactor/raw/master/trc.lua";
array_push($programs, new Program("trc", "0.1",$trc , "trc.msx", $trc));


echo json_encode($programs);

?>