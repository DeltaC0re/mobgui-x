<?php

$apis = array();

function addApi($name, $localpath, $remotepath, $osload){
    global $apis;
    array_push($apis, array("name" => "$name","localPath" => "$localpath","remotePath" => "$remotepath","osLoad" => $osload));
}

$base = "https://cmob.net/app/mb/cur/apis/";

addApi("debug", "$base" . "debug", "debug", false);
addApi("database", "$base" . "database", "database", false);
addApi("drawapi", "$base" . "drawapi", "drawapi", false);
addApi("httpapi", "$base" . "httpapi", "httpapi", false);
addApi("networkapi", "$base" . "networkapi", "networkapi", false);
addApi("termapi", "$base" . "termapi", "termapi", false);

echo json_encode($apis);

?>