<?php 
/*
    Author : MinecraftM0b (http://www.ccmob.net)
    (c) Marcel Benning 2015 - License: https://bitbucket.org/DeltaC0re/mobgui-1.3/src/master/License
*/
include("include/connector.php");
$pgms = Array();
class Dependency{
    function __construct($pName, $pVersion, $repo ,$dependencies = array()){
        $this->pName = $pName;
        $this->pVersion = $pVersion;
        $this->dependencies = $dependencies;
        $this->repo = $repo;
    }
}
class Program{
    function __construct($pName, $pVersion, $remotePath, $rawFileName , $repo ,$dependencies = array()){
        $this->pName = $pName;
        $this->pVersion = $pVersion;
        $this->remotePath = $remotePath;
        $this->dependencies = $dependencies;
        $this->rawFileName = $rawFileName;
        $this->repo = $repo;
    }
}
function resolveDependency($did){
    $dep_q = DB_QueryString("SELECT * FROM `mobgui_cur`.`cur_dependencies` WHERE id = '$did'");
    $d_deps = array();
    $row = $dep_q->fetch_assoc();
    if(!($row['dependencies_ids'] == "")){
        $d_deps_String_arr = explode(";", $row['dependencies_ids']);
        foreach ($d_deps_String_arr as $index => $dep) {
            array_push($d_deps, $dep);
        }
    }
    return new Dependency($row['name'], $row['version'], $row['repoAddress'], $d_deps);
}
$cur_query = DB_QueryString("SELECT * FROM `mobgui_cur`.`cur_programs`");
while($row = $cur_query->fetch_assoc()){
    $p_remotePath = str_replace("%rb%", $row['repoBase'], $row['remotePath']);
    $p_rawFileName = $row['fileName'];
    $p_repo = $row['repoBase']. $row['repoAddress'];
    $p_deps = array(); // todo: resolve deps
    $pgm_deps_str = $row['dependencies_ids']; 
    if(!($pgm_deps_str == "")){
        $pgm_deps_str_arr = explode(";", $pgm_deps_str);
        foreach ($pgm_deps_str_arr as $index => $dep) {
            array_push($p_deps, resolveDependency($dep));
        }
    }
    array_push($pgms, new Program($row['name'], $row['version'], $p_remotePath, $p_rawFileName, $p_repo, $p_deps));
}
echo json_encode($pgms);
?>